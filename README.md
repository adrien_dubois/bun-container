# Bun Container

bun create react [app name]

bun install @types/node @types/react @types/react-dom

Lancer le server : bun dev

Installer package: bun install [package]

Pour les react scripts : 
	- bun a react-scripts -d
	
"scripts":{
	"start": "bun dev",
	"build": "react-scripts build"
},

## CONFIG TYPESCRIPT :
```js
{
    "compilerOptions": {
      "target": "es5",
      "lib": ["dom", "dom.iterable", "esnext"],
      "allowJs": true,
      "skipLibCheck": true,
      "strict": false,
      "forceConsistentCasingInFileNames": true,
      "noEmit": true,
      "esModuleInterop": true,
      "module": "esnext",
      "moduleResolution": "node",
      "resolveJsonModule": true,
      "isolatedModules": true,
      "jsx": "preserve",
      "baseUrl": ".",
      "paths": {}
    },
    "include": ["**/*.ts", "**/*.tsx"],
    "exclude": ["node_modules"]
}
```